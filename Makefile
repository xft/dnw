ifneq ($(KERNELRELEASE), )
	obj-m := secbulk.o
else
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build
	PWD := $(shell pwd)

all: dnw driver

dnw: dnw.c secbulk.h
	$(CC) -Wall -W -O2 dnw.c -o dnw

driver:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

.PHONY: clean distclean
clean:
	rm -rf modules.* *.o *~ core .depend .*.cmd
	rm -rf *.mod.c .tmp_versions Module*

distclean: clean
	rm -rf dnw secbulk.ko

endif
