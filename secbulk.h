/*
 * samsung MCU USB dnw adaptor driver header file
 *
 * Original driver for unknown by anonymous
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License version
 *	2 as published by the Free Software Foundation.
 *
 */


#define BULKOUT_BUFFER_SIZE (1024 * 512)


/* Samsung vendor ID */
#define SAMSUNG_VENDOR_ID	0x04e8

/* Samsung sec24xx product ID */

/* Samsung s3c64xx product ID */
#define SAMSUNG_PRODUCT_ID	0x1234
